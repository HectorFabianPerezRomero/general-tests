const autprefixr = require('gulp-autoprefixer');
const gulp = require('gulp');
const plumber = require('gulp-plumber');
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const rename = require("gulp-rename");


const css = () => {
  return gulp
    .src('./scss/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: 'nested' }))
    .pipe(plumber())
    .pipe(autprefixr())
    .pipe(sourcemaps.write())
    .pipe(rename('styles.css'))
    .pipe(gulp.dest('./css/'))
};

const js = () => {
  return gulp
    .src('.js/*.js')
    .pipe(concat('general.js'))
    .pipe(gulp.dest('./'))
};

const watchFiles = () => {
  gulp.watch('./scss/**/*.scss', css);
  // gulp.watch('./js', js);
};

const watch = gulp.series(css, gulp.parallel(watchFiles));

exports.watch = watch;
exports.default = watch;
