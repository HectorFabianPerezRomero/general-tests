# Test simple rest API

This is an option to expose Rest API with a CRUD of a table, using routes.

# Installation
You only need to install like a normal module, after installing first module with the table. Automatically this module install all REST configurations for the endpoint.
# Services
The endpoint will be active in
```
http://your-drupal-url.com/test-crud/data
```
## Create
Use POST method for this option. URL
```
http://your-drupal-url.com/test-crud/data
```
The body must be:
```json
{
	"name": "Julio Rodriguez",
	"user_id": "351389432",
	"birth": 1611940281,
	"role": "Administrator"
}
```
The fields definitions:
  - Name. Only alphanumeric values
  - user_id: Only numbers
  - birth: Timestamp. Only numbers.
  - role: String. Options: Administrator, Webmaster, Developer

In case of errors, the system will return a 406 code indicating all errors in body response. An example of the possible response:

```json
{
  "status": 200,
  "message": "Row created",
  "data": {
    "body": {
      "name": "Julio Rodriguez",
      "user_id": "351389432",
      "birth": 1611940281,
      "role": "Administrator"
    },
    "id_new_row": "3"
  }
}
```
An example of error response:
```json
{
  "status": 406,
  "message": "Field error",
  "errors": [
    "The name Julio-Rodriguez does not meet the requirements",
    "The user id 351asd389432 does not meet the requirements. Only numbers is accepted"
  ]
}
```
## Get
Use GET method for this option. URL
```
http://your-drupal-url.com/test-crud/data/{ID}
```
An example of error response:
```json
{
  "data": [
    {
      "uid": "6",
      "name": "admin",
      "user_id": "351389489",
      "birth": "1609954161",
      "role": "Administrator",
      "status": 1
    }
  ]
}
```
If ID is not available, this service will return all rows.
## Update
Use PUT method for this option. URL
```
http://your-drupal-url.com/test-crud/data
```
The same definitions for the fields.
## Delete
Use DELETE method for this option. URL
```
http://your-drupal-url.com/test-crud/data
```
The same definitions for the fields.
### Module dependencies

This module uses the Drupal Rest API. This modules will be installed with this module:

* Test january 2021

In this case, the module install all needed configurations.
# Repository

This module is in a repository [here](https://bitbucket.org/HectorFabianPerezRomero/general-tests/src/master/). The Drupal in this repository in configured with ddev. You can clone the repo, execute ``` ddev start ```, import the database and you can check this Rest API

License
----

MIT
