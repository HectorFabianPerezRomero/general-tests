<?php

namespace Drupal\test_simple_rest_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class TestSimpleRestController.
 */
class TestSimpleRestController extends ControllerBase {

  /**
   * Drupal\test_january_2021\Services\TestServiceInterface definition.
   *
   * @var \Drupal\test_january_2021\Services\TestServiceInterface
   */
  protected $testService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->testService = $container->get('test_january_2021.default');
    return $instance;
  }

  /**
   * Index.
   *
   * @return string
   *   Return Hello string.
   */
  public function index($id) {
    if($id == 0)
      $id = NULL;
    $response = [
      'status' => 200,
      'message' => 'All rows',
      'data' => $this->testService->getPlainIndexData($id),
    ];
    return new JsonResponse($response);
  }

  /**
   * Create.
   *
   * @return string
   *   Return Hello string.
   */
  public function createRow(Request $request) {
    $body = json_decode($request->getContent());

    return $this->testService->processRestCreateRow((array) $body);
  }

  /**
   * Update.
   *
   * @return string
   *   Return Hello string.
   */
  public function updateRow(Request $request) {
    $body = json_decode($request->getContent());

    return $this->testService->processRestUpdateRow((array) $body);
  }

  /**
   * Delete.
   *
   * @return string
   *   Return Hello string.
   */
  public function deleteRow(Request $request) {
    $body = json_decode($request->getContent());

    return $this->testService->processRestDeleteRow((array) $body);
  }

}
